var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var rename = require('gulp-rename'); 
var autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
	return gulp.src('public/stylesheets/*.scss') 
		.pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
		.pipe(sass({
			outputStyle: 'compressed',
			includePaths: ['node_modules/susy/sass']
		}).on('error', sass.logError))
		.pipe(rename({
			basename: 'style',
			suffix: '.min'
		}))
		.pipe(gulp.dest('public/css/'))
		.pipe(browserSync.stream());
});

gulp.task('serve', ['sass'], function () {
	browserSync.init({
		server: "./public"
	});

	gulp.watch('public/stylesheets/**/*.scss', ['sass']); 
	gulp.watch("public/index.html").on('change', browserSync.reload); 
});

gulp.task('default', ['serve']); 